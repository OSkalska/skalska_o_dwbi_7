USE [labor_sql]
GO

--1.
SELECT maker, type, speed, hd
FROM pc
INNER JOIN product ON pc.model = product.model
WHERE hd <=8;

--2
SELECT DISTINCT maker
FROM pc
INNER JOIN product ON pc.model = product.model
WHERE speed >=600;

--3. 
SELECT DISTINCT maker
FROM laptop
INNER JOIN product ON laptop.model = product.model
WHERE speed <=500;

--4. 
SELECT MAX(CAST(l1.model AS INT)) AS max_mode
      ,MIN(CAST(l2.model AS INT)) AS min_model
      ,l1.hd
      ,l1.ram 
FROM [dbo].[laptop] l1
INNER JOIN [dbo].[laptop] l2 ON l1.hd = l2.hd and l1.ram = l2.ram
GROUP BY l1.model, l2.model, l1.hd, l1.ram
HAVING COUNT(l1.model) > 1;

--5.
SELECT country
FROM classes
GROUP BY country
HAVING COUNT(DISTINCT type) = 2;

--6. 
SELECT DISTINCT pc.model, maker
FROM pc
INNER JOIN product ON pc.model = product.model
WHERE price <600;

--7. 
SELECT printer.model, maker
FROM printer
INNER JOIN product ON printer.model = product.model
WHERE price >300;

--8. 
SELECT DISTINCT maker, pc.model, price
FROM pc
INNER JOIN product ON pc.model = product.model;

--9. 
SELECT  maker, pc.model, price
FROM pc
INNER JOIN product ON pc.model = product.model;

--10.
SELECT maker, type, laptop.model, speed
FROM laptop
INNER JOIN product ON laptop.model = product.model
WHERE speed > 600;

--11. 
SELECT name, displacement 
FROM ships
LEFT JOIN classes ON ships.class = classes.class;

--12. 
SELECT o.ship, b.name, b.date
FROM battles b
INNER JOIN outcomes o ON b.name = o.battle
WHERE result = 'ok';

--13.
SELECT  name, country 
FROM ships s
LEFT JOIN classes c ON s.class = c.class;

--14.
SELECT DISTINCT plane, name
FROM company
INNER JOIN trip ON company.id_comp = trip.id_comp
WHERE plane = 'Boeing';

--15. 
SELECT name, date
FROM passenger
INNER JOIN pass_in_trip ON passenger.id_psg = pass_in_trip.id_psg;

--16.
SELECT pc.model, speed, hd
FROM pc
INNER JOIN product ON pc.model = product.model
WHERE hd = 10 OR hd = 20
AND maker = 'A'
ORDER BY speed ASC;

--17.
SELECT maker, pc, laptop, printer
FROM product x PIVOT (COUNT(model)
FOR type IN(pc, laptop, printer))pvt;

--18.
SELECT [avg_],[11],[12],[14],[15]
 FROM (SELECT 'average price' AS 'avg_', screen, price FROM Laptop) x PIVOT (AVG(price)
 FOR screen
 IN([11],[12],[14],[15])) pvt;

--19. 
SELECT model, maker
FROM laptop l
CROSS APPLY (SELECT maker FROM product p
WHERE l.model = p.model) a;

--20.
SELECT *
FROM laptop l1
CROSS APPLY
(SELECT MAX(price) AS max_price FROM Laptop l2
INNER JOIN  product p1 ON l2.model=p1.model 
WHERE maker = (SELECT maker FROM Product p2 WHERE p2.model= l1.model)) a ;

--21.
SELECT *
FROM laptop l1
CROSS APPLY (SELECT TOP 1 * FROM Laptop l2  
WHERE l1.model < l2.model OR (l1.model = l2.model AND l1.code < l2.code) 
ORDER BY model, code) X
ORDER BY l1.model;

--22. 
SELECT *
FROM laptop l1
OUTER APPLY (SELECT TOP 1 * FROM Laptop l2  
WHERE l1.model < l2.model OR (l1.model = l2.model AND l1.code < l2.code) 
ORDER BY model, code) X
ORDER BY l1.model;

--23
SELECT b.* 
FROM (SELECT DISTINCT type FROM product) prod1
CROSS APPLY(SELECT top 3 * FROM product prod2
WHERE prod1.type=prod2.type
ORDER BY prod2.model) b;

--24. 
SELECT code, name, value 
FROM laptop
CROSS APPLY
(VALUES('speed', speed) ,('ram', ram), ('hd', hd), ('screen', screen)) a (name, value)
ORDER BY code, name, value;

