------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS O_S_module_3
GO
------Create New Database------
CREATE DATABASE [O_S_module_3]
GO

------Select New-created Database------
USE O_S_module_3
GO

------Create Parent table------
CREATE TABLE [dbo].[customers]
(
 [customer_id]   INT NOT NULL ,
 [company_name]  NVARCHAR(50) NOT NULL ,
 [contact_name]  NVARCHAR(30) NOT NULL ,
 [address]       NVARCHAR(60) NOT NULL ,
 [city]          NVARCHAR(30) NOT NULL ,
 [region]        NVARCHAR(30) NOT NULL ,
 [postal_code]   NVARCHAR(10) NOT NULL ,
 [country]       NVARCHAR(15) NOT NULL ,
 [phone]         NVARCHAR(24) NOT NULL ,
 [fax]           NVARCHAR(24) NOT NULL ,
 [inserted_date] DATETIME NOT NULL DEFAULT GETDATE(),
 [updated_date]  DATETIME NOT NULL DEFAULT GETDATE(),
 ------Define Primary Key with Clustered Index------
 CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED ([customer_id] ASC),
 ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT phone_uk UNIQUE ([phone])
)
GO

--DROP TABLE orders

------Create Child table------
CREATE TABLE [dbo].[orders]
(
 [order_id]         INT NOT NULL ,
 [order_date]       DATETIME NOT NULL ,
 [weight]           DECIMAL(10,3) NOT NULL ,
 [shipping_type]    NVARCHAR(10) NOT NULL ,
 [carrier_name]     NVARCHAR(60) NOT NULL ,
 [ship_address]     NVARCHAR(60) NOT NULL ,
 [ship_city]        NVARCHAR(50) NOT NULL ,
 [ship_postal_code] NVARCHAR(10) NOT NULL ,
 [ship_country]     NVARCHAR(15) NOT NULL ,
 [inserted_date]    DATETIME NOT NULL DEFAULT GETDATE(),
 [updated_date]     DATETIME NOT NULL DEFAULT GETDATE(),
 [customer_id]      INT NOT NULL ,
 ------Check weight column------
	   CONSTRAINT weight_chk CHECK ([weight] >=0),
------Define Primary and Foreign keys------ 
 CONSTRAINT [PK_orders] PRIMARY KEY CLUSTERED ([order_id] ASC),
 CONSTRAINT [FK_45] FOREIGN KEY ([customer_id])
  REFERENCES [dbo].[customers]([customer_id])
)
GO


-----Data Preparation for Parent Table------
INSERT INTO [dbo].[customers]
          ( [customer_id]   
           ,[company_name]  
           ,[contact_name]   
           ,[address]       
           ,[city]          
           ,[region]        
           ,[postal_code]   
           ,[country]      
           ,[phone]         
           ,[fax]           
           ,[inserted_date] 
           ,[updated_date]
		   ) 
	 VALUES
	      (1
		  ,'Express'
		  ,'Leon Smith'
		  ,'Lysenka 35/4'
		  ,'Lviv'
		  ,'Pustomyty district'
		  ,79100
		  ,'Nepal'
		  ,'(032)2514115'
		  ,'(032)2514116'
		  ,''
		  ,''
		  )
GO

----insert new record, set valid value to weight---- 
INSERT INTO [dbo].[orders]
           ([order_id] 
           ,[order_date]
           ,[weight]
		   ,[shipping_type] 
           ,[carrier_name]
		   ,[ship_address]
		   ,[ship_city] 
		   ,[ship_postal_code]
		   ,[ship_country]
		   ,[inserted_date] 
		   ,[updated_date] 
		   ,[customer_id]
		     )
     VALUES
           (1
           ,'2018-07-16'
           ,0
		   ,'train'
		   ,'intercity'
		   ,'kytajska 2'
		   ,'Lviv'
		   ,'79038'
		   ,'Gonduras'
		   ,DEFAULT
		   ,DEFAULT
		   ,'1'
		   )
GO

------Update table [orders], set invalid value to weight------ 
UPDATE [dbo].[orders]
SET [weight] = -1
GO

------Dataset verification------
SELECT * FROM orders
GO

------Create trigger------
USE [O_S_module_3]
GO

CREATE TRIGGER customers_updated_date
ON customers
AFTER UPDATE
AS
BEGIN
      UPDATE customers
	  SET  updated_date = GETDATE()
	  WHERE customer_id IN (SELECT DISTINCT customer_id FROM customers)
    END;




------Create view_customers for customers_table------
USE [O_S_module_3]
GO

CREATE VIEW view_customers
AS
SELECT company_name, contact_name, address, city, region, postal_code, country, phone, fax
FROM customers
WHERE city = 'Lviv';

SELECT *
FROM view_customers;


------Create view_orders for orders_table------
USE [O_S_module_3]
GO

CREATE VIEW view_orders
AS
SELECT weight, carrier_name 
FROM orders
WHERE carrier_name LIKE 'i%'; 

SELECT *
FROM view_orders;


------create view wiht check option------
USE [O_S_module_3]
GO
CREATE VIEW view_customers_Nepal
AS
select *
from customers
WHERE postal_code=79100
WITH CHECK OPTION































    






