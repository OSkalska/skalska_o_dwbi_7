------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS education
GO
------Create New Database------
CREATE DATABASE [education]
GO

------Select New-created Database------
USE [education]
GO

------Create Schema-----
CREATE SCHEMA O_Skalska
GO

------Create Synonym for [dbo].[booking_information]------
CREATE SYNONYM [O_Skalska].[booking_information] FOR [O_S_module_4].[dbo].[booking_information]
GO
------Create Synonym for [dbo].[booking_information_2]------
CREATE SYNONYM [O_Skalska].[booking_information_2] FOR [O_S_module_4].[dbo].[booking_information_2]
GO