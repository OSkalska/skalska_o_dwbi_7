USE O_S_module_4
GO
-----Check constraint for final_price-----
INSERT INTO [dbo].[booking_information]
 (
  [arrival_date] 
 ,[depart_time]  
 ,[adults]    
 ,[children]    
 ,[final_price]   
 ,[customer_first_name] 
 ,[customer_last_name]  
 ,[country]        
 ,[post_code]       
 ,[city]       
 ,[customer_email_address]
 ,[payment_method]  
 ,[hotel_id]
 ,[updated] 
 )
 VALUES
 ('2018-07-23'
 ,'2018-07-22'
 ,2
 ,12
 ,0
 ,'Pedro'
 ,'Ramirez'
 ,'Atlantis'
 ,'79250'
 ,'Komarno'
 ,'user@test.com'
 ,'cash'
 ,'666'
 ,'2018-07-20')
GO
-----Check constraint for customer_email_address-----
INSERT INTO [dbo].[booking_information]
 (
  [arrival_date] 
 ,[depart_time]  
 ,[adults]    
 ,[children]    
 ,[final_price]   
 ,[customer_first_name] 
 ,[customer_last_name]  
 ,[country]        
 ,[post_code]       
 ,[city]       
 ,[customer_email_address]
 ,[payment_method]  
 ,[hotel_id]
 ,[updated] 
 )
 VALUES
 ('2018-07-23'
 ,'2018-07-22'
 ,2
 ,12
 ,1000.50
 ,'Pedro'
 ,'Ramirez'
 ,'Atlantis'
 ,'79250'
 ,'Komarno'
 ,'usertest.com'
 ,'cash'
 ,'666'
 ,'2018-07-20')
GO
-----Correct insertion-----
INSERT INTO [dbo].[booking_information]
 (
  [arrival_date] 
 ,[depart_time]  
 ,[adults]    
 ,[children]    
 ,[final_price]   
 ,[customer_first_name] 
 ,[customer_last_name]  
 ,[country]        
 ,[post_code]       
 ,[city]       
 ,[customer_email_address]
 ,[payment_method]  
 ,[hotel_id]
 ,[updated] 
 )
 VALUES
 ('2018-07-23'
 ,'2018-07-22'
 ,2
 ,12
 ,1000.50
 ,'Pedro'
 ,'Ramirez'
 ,'Atlantis'
 ,'79250'
 ,'Komarno'
 ,'user@test.com'
 ,'cash'
 ,'666'
 ,'2018-07-20')
GO

SELECT * FROM [dbo].[booking_information] 
