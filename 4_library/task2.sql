------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS O_S_module_4
GO
------Create New Database------
CREATE DATABASE [O_S_module_4]
GO

------Select New-created Database------
USE O_S_module_4
GO

------create table[dbo].[booking_information]------

CREATE TABLE [dbo].[booking_information]
(
 [id]                     INT NOT NULL IDENTITY,
 [arrival_date]           DATETIME NOT NULL ,
 [depart_time]            DATETIME NOT NULL ,
 [adults]                 INT NOT NULL ,
 [children]               INT NOT NULL ,
 [final_price]            DECIMAL(24,2) NOT NULL ,
 [customer_first_name]    VARCHAR(50) NOT NULL ,
 [customer_last_name]     VARCHAR(50) NOT NULL ,
 [country]                VARCHAR(60) NOT NULL ,
 [post_code]              VARCHAR(24) NOT NULL ,
 [city]                   VARCHAR(50) NOT NULL ,
 [customer_email_address] VARCHAR(50) NULL ,
 [payment_method]         VARCHAR(50) NOT NULL ,
 [hotel_id]               INT NOT NULL ,
 [updated]                DATETIME NOT NULL,

 CONSTRAINT [customer_email_address] CHECK ([customer_email_address] like '%@%.%'),
 CONSTRAINT [final_price] CHECK ([final_price] > 0)
);

GO

------create similar table[dbo].[booking_information]------

CREATE TABLE [dbo].[booking_information_2]
(
 [id]                     INT NOT NULL IDENTITY,
 [arrival_date]           DATETIME NOT NULL ,
 [depart_time]            DATETIME NOT NULL ,
 [adults]                 INT NOT NULL ,
 [children]               INT NOT NULL ,
 [final_price]            DECIMAL(24,0) NOT NULL ,
 [customer_first_name]    VARCHAR(50) NOT NULL ,
 [customer_last_name]     VARCHAR(50) NOT NULL ,
 [country]                VARCHAR(60) NOT NULL ,
 [post_code]              VARCHAR(24) NOT NULL ,
 [city]                   VARCHAR(50) NOT NULL ,
 [customer_email_address] VARCHAR(50) NULL ,
 [payment_method]         VARCHAR(50) NOT NULL ,
 [hotel_id]               INT NOT NULL ,
 [type_operation]         VARCHAR(50) NOT NULL ,
 [date_operation]         DATETIME NOT NULL ,
);

GO

------create insert trigger------
CREATE TRIGGER booking_information_INSERT
ON booking_information
AFTER INSERT
AS
BEGIN
UPDATE [dbo].[booking_information]
SET [updated] = GETDATE() 
WHERE id IN (SELECT DISTINCT id FROM [booking_information])
END 
GO

------create update trigger------
CREATE TRIGGER booking_information_UPDATE
ON booking_information
AFTER UPDATE
AS
BEGIN
UPDATE [dbo].[booking_information]
SET [updated] = GETDATE() 
WHERE id IN (SELECT DISTINCT id FROM [booking_information])
END 
GO

------create delete trigger------
CREATE TRIGGER booking_information_DELETE
ON booking_information
INSTEAD OF DELETE
AS
BEGIN
RAISERROR ('This table is protected',16,1)
ROLLBACK TRANSACTION
END 
GO
