

USE [labor_sql]
GO


--1.
SELECT row_number() OVER (ORDER BY id_comp, trip_no)  AS number,
trip_no, id_comp 
FROM trip
ORDER BY id_comp, trip_no;

--2.
SELECT row_number() OVER (PARTITION BY id_comp ORDER BY id_comp, trip_no ASC) AS 'number', trip_no, id_comp
FROM trip
ORDER BY id_comp, trip_no;

--3
--4
SELECT maker
FROM product
where type ='pc'
GROUP BY maker
HAVING COUNT( type) > 2;
--5
--6
--7
--8
--9