USE people_ua_db
GO

SELECT 
	  ix.[name] AS 'index'
	 ,OBJECT_NAME(ips.[object_id]) AS 'table' 
     ,ips.[index_type_desc] 
     ,ips.[avg_fragmentation_in_percent] 
     ,ips.[fragment_count] 
FROM  sys.dm_db_index_physical_stats
      (
	   DB_ID(N'people_ua_db')
	  ,NULL
	  ,NULL
	  ,NULL
	  ,NULL
	  ) ips
	INNER JOIN sys.indexes ix  
	ON  ips.[object_id] = ix.[object_id]
GO

-------reorganize in case fragmentation <= 30%----------
ALTER INDEX PK_surname_list_identity
ON [dbo].[surname_list_identity]
REORGANIZE
GO

-------rebuild in case fragmantation > 30%-----------
ALTER INDEX PK_surname_list_identity
ON [dbo].[surname_list_identity]
REBUILD
GO
