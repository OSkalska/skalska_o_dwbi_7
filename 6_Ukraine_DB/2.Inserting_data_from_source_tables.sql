USE [people_ua_db]
GO

DECLARE @men TABLE 
                 (id int identity
				 ,name varchar(50))
INSERT INTO @men
SELECT [name] 
FROM [dbo].[name_list_identity]
WHERE sex = 'm'

DECLARE @women TABLE 
                 (id int identity
				 ,[name] varchar(50))
INSERT INTO @women
SELECT [name] 
FROM [dbo].[name_list_identity]
WHERE sex = 'w'

DECLARE @count INT
DECLARE @area_id INT
DECLARE @name_count INT 
SET @name_count = (SELECT COUNT(*) FROM [dbo].[name_list_identity])
DECLARE @men_count INT 
SET @men_count = (SELECT COUNT(*) from @men)
DECLARE @women_count INT 
SET @women_count = (SELECT COUNT(*) from @women)
DECLARE @random TABLE
                  (id INT IDENTITY 
				  ,[name] VARCHAR(50)
				  ,[sex] NCHAR(1))
DECLARE @day_of_birth VARCHAR(10)
DECLARE @year INT
DECLARE @month INT
DECLARE @day INT 
DECLARE @days_amount int 
SET @days_amount = 31
DECLARE @surname VARCHAR(50)
DECLARE @amount INT
DECLARE @sex NCHAR(1)
DECLARE @name VARCHAR(50)
DECLARE @random_id INT

DECLARE [processing] CURSOR 
FOR 
SELECT 
 [surname]
,[sex]
,[amount] 
FROM [dbo].[surname_list_identity]

OPEN [processing]
FETCH NEXT 
      FROM [processing] into @surname, @sex , @amount
      WHILE @@FETCH_STATUS = 0
	    BEGIN
	      WHILE @amount > 0
		    BEGIN
		       SET @year  = (1925 + (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) %99
                  FROM sysobjects x
                  CROSS JOIN sysobjects y))
                  SET @month = (1 + (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) % 12
                  FROM sysobjects x
                  CROSS JOIN sysobjects y))
			         IF @month = 2
                        BEGIN
				           SET @days_amount = 28
				        END
                  SET @day = (1+ (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) % @days_amount
                  FROM sysobjects x
                  CROSS JOIN sysobjects y))
	              SET @day_of_birth = CONCAT(@year,'-',@month ,'-',@day) 
			      PRINT @day_of_birth
                  SET @area_id = (1+ (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) % (SELECT COUNT(*) FROM [dbo].[Area])
                  FROM sysobjects x
                  CROSS JOIN sysobjects y))
	                 IF @sex != NULL
	                    BEGIN
	                       IF @sex = 'm'
		                      BEGIN
		                        SET @name = (SELECT [name] FROM @men WHERE id = (1+(SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID())))))% @men_count
                                FROM sysobjects x
                                CROSS JOIN sysobjects y))) 
		                      END
		                   IF @sex = 'w'
		                      BEGIN
		                        SET @name = (SELECT [name] FROM @women WHERE id = (1+(SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID())))))% @women_count
                                FROM sysobjects x
                                CROSS JOIN sysobjects y)))
		                      END
	                    END
	                 ELSE
	                    BEGIN
	                      SET @random_id  = (1+(SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID())))))% @name_count
                          FROM sysobjects x
                          CROSS JOIN sysobjects y))
                          SET @name = (SELECT [name] FROM [dbo].[name_list_identity] WHERE id = @random_id)
	                      SET @sex  = (SELECT [sex] FROM [dbo].[name_list_identity] WHERE id = @random_id)
	                    END
		       INSERT INTO [dbo].[Census]
			           (
						[name]
					   ,[surname]
					   ,[sex]
					   ,[day_of_birth]
					   ,[Area_id]
					   ) 
			          VALUES
		                  (
					       @name
					      ,@surname
					      ,@sex
					      ,@day_of_birth
					      ,@area_id
					      )
	           SET @count += 1	
			       IF @count = 10000
				       SET @count = 0
                       SET @amount -= 1
		   END
	      FETCH NEXT 
		  FROM [processing] 
		  INTO @surname
		      ,@sex 
			  ,@amount 
	    END
CLOSE [processing]
DEALLOCATE [processing];
GO

SELECT * FROM [dbo].[Census]