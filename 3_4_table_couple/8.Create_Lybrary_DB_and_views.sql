------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS O_Skalska_Library_view
GO
------Create New Database------
CREATE DATABASE [O_Skalska_Library_view]
GO

------Create view_customers for Authors_log_table------
USE [O_Skalska_Library_view]
GO

CREATE VIEW view_Authors_log
AS
SELECT *
FROM O_Skalska_Library.dbo.Authors_log
GO
------Create view_customers for Publishers_log_table------
USE [O_Skalska_Library_view]
GO

CREATE VIEW view_Publishers
AS
SELECT *
FROM O_Skalska_Library.dbo.Publishers;
GO

------Create view_customers for Authors_table------
USE [O_Skalska_Library_view]
GO

CREATE VIEW view_Authors
AS
SELECT *
FROM O_Skalska_Library.dbo.Authors;
GO

------Create view_customers for Books table------
USE [O_Skalska_Library_view]
GO

CREATE VIEW view_Books
AS
SELECT *
FROM O_Skalska_Library.dbo.Books;
GO


------Create view_customers for BooksAuthors table------
USE [O_Skalska_Library_view]
GO

CREATE VIEW view_BooksAuthors 
AS
SELECT *
FROM O_Skalska_Library.dbo.BooksAuthors;
GO











