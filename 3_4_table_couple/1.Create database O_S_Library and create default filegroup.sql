------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS O_Skalska_Library
GO
------Create New Database------
CREATE DATABASE [O_Skalska_Library]
GO

/*create new file group*/
USE [master]
GO

ALTER DATABASE [O_Skalska_Library] ADD FILEGROUP [Data]
GO

/*create new data file */
USE [master]
GO

ALTER DATABASE [O_Skalska_Library] 
ADD FILE ( NAME = N'O_Skalska_Library_data', 
    FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\O_Skalska_Library_data.mdf' , 
    SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
GO

/*set default file group*/
USE [O_Skalska_Library]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
	ALTER DATABASE [O_Skalska_Library] MODIFY FILEGROUP [Data] DEFAULT
GO

------Select New-created Database------
USE [O_Skalska_Library]
GO