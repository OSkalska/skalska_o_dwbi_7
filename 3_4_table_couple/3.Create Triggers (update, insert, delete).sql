USE O_Skalska_Library
GO
------create delete-restriction trigger for Authors_log-------

DROP TRIGGER IF EXISTS [Authors_log_DELETE_Restriction]
GO

CREATE TRIGGER Authors_log_DELETE_Restriction
ON [Authors_log]
INSTEAD OF DELETE
AS
BEGIN
RAISERROR ('This table is protected',16,1)
ROLLBACK TRANSACTION
END 
GO

------create trigger on Update for Authors------

DROP TRIGGER IF EXISTS [AUTHORS_Updated]
GO

CREATE TRIGGER AUTHORS_Updated
ON [Authors]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[Authors]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
WHERE [Author_Id] IN (SELECT DISTINCT [Author_Id] FROM Inserted)
END 
GO

------create trigger on Update for Books------

DROP TRIGGER IF EXISTS [Books_Updated]
GO

CREATE TRIGGER Books_Updated
ON [Books]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[Books]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
WHERE [ISBN] IN (SELECT DISTINCT [ISBN] FROM Inserted)
END 
GO

------create trigger on Update for BooksAuthors------

DROP TRIGGER IF EXISTS [BooksAuthors_Updated]
GO

CREATE TRIGGER BooksAuthors_Updated
ON [BooksAuthors]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[BooksAuthors]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
WHERE [BooksAuthors_id] IN (SELECT DISTINCT [BooksAuthors_id] FROM Inserted)
END 
GO

------create trigger on Update for Publishers------

DROP TRIGGER IF EXISTS [Publishers_Updated]
GO

CREATE TRIGGER Publishers_Updated
ON [Publishers]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[Publishers]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER 
WHERE [Publisher_Id] IN (SELECT DISTINCT [Publisher_Id] FROM Inserted)
END 
GO

------update audit columns trigger for Authors_log / Insertion-----

DROP TRIGGER IF EXISTS [author_log_insert]
GO

CREATE TRIGGER author_log_insert
ON [Authors]
AFTER INSERT
AS
	INSERT INTO [Authors_log] 
 (
  [Author_Id_new]
 ,[Name_new]
 ,[URL_new]
 ,[operation_type]
 )
	SELECT author_id, [name], [url], 'I' FROM inserted
GO

------update audit columns trigger for Authors_log / Deleting-----

DROP TRIGGER IF EXISTS [author_log_Delete]
GO

CREATE TRIGGER author_log_Delete
ON [Authors]
AFTER Delete
AS
	INSERT INTO [Authors_log] 
 (
  [Author_Id_old]
 ,[Name_old] 
 ,[URL_old]
 ,[operation_type]
 )
	SELECT author_id, [name], [url], 'D' FROM deleted
GO

------update audit columns trigger for Authors_log / Updating-----

DROP TRIGGER IF EXISTS [author_log_Update]
GO

CREATE TRIGGER author_log_Update
ON [Authors]
AFTER Update
AS
	INSERT INTO [Authors_log]
	(
	[Author_Id_new]
   ,[Name_new]  
   ,[URL_new] 
   ,[Author_Id_old]
   ,[Name_old] 
   ,[URL_old] 
   ,[operation_type]
   ) 
   SELECT 
   inserted.[Author_Id]
  ,inserted.[Name]
  ,inserted.[URL]
  ,deleted.[Author_Id]
  ,deleted.[Name]
  ,deleted.[URL]
  ,'U'
  FROM inserted, deleted
 GO  
