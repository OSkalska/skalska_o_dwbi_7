 ------Create 20 records for each table------
 USE O_Skalska_Library
 GO

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log

INSERT INTO O_Skalska_Library.dbo.Authors 
([Name], [URL])
VALUES 
('Author1' , 'www.author1.com'),
('Author2' , 'www.author2.com'),
('Author3' , 'www.author3.com'),
('Author4' , 'www.author4.com'),
('Author5' , 'www.author5.com'),
('Author6' , 'www.author6.com'),
('Author7' , 'www.author7.com'),
('Author8' , 'www.author8.com'),
('Author9' , 'www.author9.com'),
('Author10' , 'www.autho10.com'),
('Author11' , 'www.author11.com'),
('Author12' , 'www.author12.com'),
('Author13' , 'www.author13.com'),
('Author14' , 'www.author14.com'),
('Author15' , 'www.author15.com'),
('Author16' , 'www.author16.com'),
('Author17' , 'www.author17.com'),
('Author18' , 'www.author18.com'),
('Author19' , 'www.author19.com'),
('Author20' , 'www.author20.com')
GO

INSERT INTO O_Skalska_Library.dbo.Publishers
([Name], [URL])
VALUES
('Publisher1' , 'www.publisher1.com'),
('Publisher2' , 'www.publisher2.com'),
('Publisher3' , 'www.publisher3.com'),
('Publisher4' , 'www.publisher4.com'),
('Publisher5' , 'www.publisher5.com'),
('Publisher6' , 'www.publisher6.com'),
('Publisher7' , 'www.publisher7.com'),
('Publisher8' , 'www.publisher8.com'),
('Publisher9' , 'www.publisher9.com'),
('Publisher10' , 'www.publisher10.com'),
('Publisher11' , 'www.publisher11.com'),
('Publisher12' , 'www.publisher12.com'),
('Publisher13' , 'www.publisher13.com'),
('Publisher14' , 'www.publisher14.com'),
('Publisher15' , 'www.publisher15.com'),
('Publisher16' , 'www.publisher16.com'),
('Publisher17' , 'www.publisher17.com'),
('Publisher18' , 'www.publisher18.com'),
('Publisher19' , 'www.publisher19.com'),
('Publisher20' , 'www.publisher20.com')
GO


INSERT INTO O_Skalska_Library.dbo.Books
([ISBN], [Publisher_Id], [URL], [Price])
VALUES
('123456789', 1, 'www.publisher1.com', 10),
('123456788', 2, 'www.publisher2.com', 20),
('123456787', 3, 'www.publisher3.com', 30),
('123456786', 4, 'www.publisher4.com', 5),
('123456785', 5, 'www.publisher5.com', 2),
('123456784', 6, 'www.publisher6.com', 10),
('123456783', 7, 'www.publisher7.com', 12),
('123456782', 8, 'www.publisher8.com', 16),
('123456781', 9, 'www.publisher9.com', 105),
('123456780', 10, 'www.publisher10.com', 99),
('123456779', 11, 'www.publisher11.com', 23),
('123456769', 12, 'www.publisher12.com', 98),
('123456759', 13, 'www.publisher13.com', 20),
('123456749', 14, 'www.publisher14.com', 11),
('123456739', 15, 'www.publisher15.com', 26),
('123456729', 16, 'www.publisher16.com', 33),
('123456719', 17, 'www.publisher17.com', 77),
('123456709', 18, 'www.publisher18.com', 56),
('123456889', 19, 'www.publisher19.com', 32),
('123456989', 20, 'www.publisher20.com', 5)
GO

INSERT INTO O_Skalska_Library.dbo.BooksAuthors
([BooksAuthors_id], [ISBN] ,[Author_Id] ,[Seq_No])
VALUES
(1, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=1), 1, 1),
(2, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=2), 2, 2),
(3, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=3), 3, 3),
(4, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=4), 4, 4),
(5, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=5), 5, 5),
(6, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=6), 6, 6),
(7, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=7), 7, 7),
(8, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=8), 8, 8),
(9, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=9), 9, 9),
(10, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=10), 10, 10),
(11, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=11), 11, 11),
(12, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=12), 12, 12),
(13, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=13), 13, 13),
(14, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=14), 14, 14),
(15, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=15), 15, 15),
(16, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=16), 16, 16),
(17, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=17), 17, 17),
(18, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=18), 18, 18),
(19, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=19), 19, 19),
(20, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=20), 20, 20)
GO

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log
