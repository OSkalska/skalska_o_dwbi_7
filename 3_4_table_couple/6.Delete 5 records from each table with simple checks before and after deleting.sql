-----Delete 5 records for each table-----

USE O_Skalska_Library
GO

----CHECK before deleting-----
--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log

DELETE FROM O_Skalska_Library.dbo.Authors
WHERE [Name] = 'Writer1'
GO
DELETE FROM O_Skalska_Library.dbo.Authors
WHERE [Name] = 'Writer2'
GO
DELETE FROM O_Skalska_Library.dbo.Authors
WHERE [Name] = 'Writer3'
GO
DELETE FROM O_Skalska_Library.dbo.Authors
WHERE [Name] = 'Writer4'
GO
DELETE FROM O_Skalska_Library.dbo.Authors
WHERE [Name] = 'Writer5'
GO


DELETE FROM O_Skalska_Library.dbo.Publishers
WHERE [Name] = 'Product1'
GO
DELETE FROM O_Skalska_Library.dbo.Publishers
WHERE [Name] = 'Product2'
GO
DELETE FROM O_Skalska_Library.dbo.Publishers
WHERE [Name] = 'Product3'
GO
DELETE FROM O_Skalska_Library.dbo.Publishers
WHERE [Name] = 'Product4'
GO
DELETE FROM O_Skalska_Library.dbo.Publishers
WHERE [Name] = 'Product5'
GO


DELETE FROM O_Skalska_Library.dbo.Books 
WHERE [Price] >= 50
GO
DELETE FROM O_Skalska_Library.dbo.Books 
WHERE [Price] <= 50
GO
DELETE FROM O_Skalska_Library.dbo.Books 
WHERE [Price] > 50
GO
DELETE FROM O_Skalska_Library.dbo.Books 
WHERE [Price] < 50
GO
DELETE FROM O_Skalska_Library.dbo.Books 
WHERE [Price] != 50
GO

DELETE FROM O_Skalska_Library.dbo.BooksAuthors
WHERE Author_Id = 2
GO
DELETE FROM O_Skalska_Library.dbo.BooksAuthors
WHERE Author_Id = 10
GO
DELETE FROM O_Skalska_Library.dbo.BooksAuthors
WHERE Author_Id = 100
GO
DELETE FROM O_Skalska_Library.dbo.BooksAuthors
WHERE Author_Id = -3
GO
DELETE FROM O_Skalska_Library.dbo.BooksAuthors
WHERE Author_Id = 5
GO


DELETE FROM O_Skalska_Library.dbo.Authors_log
WHERE Author_Id_new = 5
GO
DELETE FROM O_Skalska_Library.dbo.Authors_log
WHERE Author_Id_new = 10
GO
DELETE FROM O_Skalska_Library.dbo.Authors_log
WHERE Author_Id_new = 70
GO
DELETE FROM O_Skalska_Library.dbo.Authors_log
WHERE Author_Id_new = 6
GO
DELETE FROM O_Skalska_Library.dbo.Authors_log
WHERE Author_Id_new = 3
GO
----CHECk after deleting-----

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log
