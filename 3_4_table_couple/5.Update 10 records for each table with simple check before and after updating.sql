USE O_Skalska_Library
GO

-----Update 10 records for each table-----

--CHECK BEFORE

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log

UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer1'
WHERE [Name] = 'Author1'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer2'
WHERE [Name] = 'Author2'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer3'
WHERE [Name] = 'Author3'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer4'
WHERE [Name] = 'Author4'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer5'
WHERE [Name] = 'Author5'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer6'
WHERE [Name] = 'Author6'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer7'
WHERE [Name] = 'Author7'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer8'
WHERE [Name] = 'Author8'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer9'
WHERE [Name] = 'Author9'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer10'
WHERE [Name] = 'Author10'
GO


UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product1'
WHERE [Name] = 'Publisher1'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product2'
WHERE [Name] = 'Publisher2'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product3'
WHERE [Name] = 'Publisher3'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product4'
WHERE [Name] = 'Publisher4'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product5'
WHERE [Name] = 'Publisher5'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product6'
WHERE [Name] = 'Publisher6'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product7'
WHERE [Name] = 'Publisher7'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product8'
WHERE [Name] = 'Publisher8'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product9'
WHERE [Name] = 'Publisher9'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product10'
WHERE [Name] = 'Publisher10'
GO


UPDATE O_Skalska_Library.dbo.Books 
SET [Price] = [Price]*1.5
GO

UPDATE O_Skalska_Library.dbo.BooksAuthors
SET [Seq_No] = 100+[Seq_No]
GO

-- CHECK AFTER Updating----

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log