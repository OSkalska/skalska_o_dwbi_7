USE O_Skalska_Library
GO

-------Create Authors_log table------

DROP TABLE IF EXISTS [Authors_log]
GO

CREATE TABLE [Authors_log]
(
 [operation_id]       INT NOT NULL IDENTITY ,
 [Author_Id_new]      INT NULL ,
 [Name_new]           VARCHAR(50) NULL ,
 [URL_new]            VARCHAR(50) NULL ,
 [Author_Id_old]      INT NULL ,
 [Name_old]           VARCHAR(50) NULL ,
 [URL_old]            VARCHAR(50) NULL ,
 [operation_type]     VARCHAR(50) NOT NULL ,
 [operation_datetime] DATETIME NOT NULL DEFAULT getdate(),
 ------Check BooksAuthors_id column------
 CONSTRAINT operation_type_chk CHECK ([operation_type] in ('I','D','U')
),

  ------Define Primary Key with Clustered Index------
 CONSTRAINT [PK_Authors_log] PRIMARY KEY CLUSTERED ([operation_id] ASC)
);
GO


-------Create Publishers table------

DROP TABLE IF EXISTS [Publishers]
GO

CREATE TABLE [Publishers]
(
 [Publisher_Id] INT NOT NULL IDENTITY,
 [Name]         VARCHAR(50) NOT NULL ,
 [URL]          VARCHAR(50) NOT NULL DEFAULT 'www.publisher_name.com',
 [inserted]     DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by]  VARCHAR(50) NOT NULL DEFAULT system_user,
 [updated]      DATE NULL ,
 [updated_by]   VARCHAR(50) NULL ,

 
  ------Define Primary Key with Clustered Index------

 CONSTRAINT [PK_Publishers] PRIMARY KEY CLUSTERED ([Publisher_Id] ASC),

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT Name_uk UNIQUE ([Name])
)
;
GO

------Create Sequence for Publisher_id-------
CREATE SEQUENCE publisher_seq 
AS INT
START WITH 10
INCREMENT BY 10
GO


-------Create Authors table------

DROP TABLE IF EXISTS [Authors]
GO

CREATE TABLE [dbo].[Authors]
(
 [Author_Id]   INT NOT NULL IDENTITY,
 [Name]        VARCHAR(50) NOT NULL ,
 [URL]         VARCHAR(50) NOT NULL DEFAULT 'www.author_name.com',
 [inserted]    DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by] TEXT NOT NULL DEFAULT SYSTEM_USER,
 [updated]     DATE NULL ,
 [updated_by]  TEXT NULL ,

 
  ------Define Primary Key with Clustered Index------

 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED ([Author_Id] ASC),

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT Name_Author_uk UNIQUE ([Name])
);
GO

------Create Sequence for Author_id-------
CREATE SEQUENCE author_seq 
AS INT
START WITH 100
INCREMENT BY 5
GO

-------Create Books table------

DROP TABLE IF EXISTS [Books]
GO

CREATE TABLE [Books]
(
 [ISBN]         VARCHAR(100) NOT NULL,
 [URL]          VARCHAR(50) NOT NULL ,
 [Price]        DECIMAL(18,0) NOT NULL DEFAULT 0,
 [inserted]     DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by]  VARCHAR(50) NOT NULL DEFAULT system_user,
 [updated]      DATE NULL ,
 [updated_by]   VARCHAR(50) NULL ,
 [Publisher_Id] INT NOT NULL ,
 ------Check price column------
 CONSTRAINT price_chk CHECK ([price] >=0),

 
  ------Define Primary Key with Clustered Index and Foreign key------
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED ([ISBN] ASC),
 CONSTRAINT [FK_80] FOREIGN KEY ([Publisher_Id])
 REFERENCES [Publishers]([Publisher_Id]),

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT URL_uk UNIQUE ([URL])
);
GO


-------Create BooksAuthors table------

DROP TABLE IF EXISTS [BooksAuthors]
GO

CREATE TABLE [BooksAuthors]
(
 [BooksAuthors_id] INT NOT NULL DEFAULT 1,
 [Seq_No]          INT NOT NULL DEFAULT 1,
 [inserted]        DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by]     VARCHAR(50) NOT NULL DEFAULT system_user,
 [updated]         DATE NOT NULL DEFAULT GETDATE(),
 [updated_by]      VARCHAR(50) NULL ,
 [ISBN]            VARCHAR(100) NOT NULL ,
 [Author_Id]       INT NOT NULL ,
 ------Check BooksAuthors_id column------
 CONSTRAINT BooksAuthors_id_chk CHECK ([BooksAuthors_id] >=1),
 CONSTRAINT Seq_Nod_chk CHECK ([Seq_No] >=1),

   ------Define Primary Key with Clustered Index and Foreign key------

 CONSTRAINT [PK_BooksAuthors] PRIMARY KEY CLUSTERED ([BooksAuthors_id] ASC),
 CONSTRAINT [FK_84] FOREIGN KEY ([ISBN])
  REFERENCES [Books]([ISBN])
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
 CONSTRAINT [FK_92] FOREIGN KEY ([Author_Id])
  REFERENCES [dbo].[Authors]([Author_Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT Author_Id_uk UNIQUE ([Author_Id]),
 CONSTRAINT ISBN_uk UNIQUE ([ISBN])

);
GO