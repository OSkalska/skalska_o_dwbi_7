------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS [O_Skalska_Library_synonym]
GO
------Create New Database------
CREATE DATABASE [O_Skalska_Library_synonym]
GO

------Select New-created Database------
USE [O_Skalska_Library_synonym]
GO

------Create Synonym for [dbo].[Authors]------
CREATE SYNONYM [dbo].[Authors] FOR [O_Skalska_Library].[dbo].[Authors]
GO
------Create Synonym for [dbo].[Books]------
CREATE SYNONYM [dbo].[Books] FOR [O_Skalska_Library].[dbo].[Books]
GO
------Create Synonym for [dbo].[BooksAuthors]------
CREATE SYNONYM [dbo].[BooksAuthors] FOR [O_Skalska_Library].[dbo].[BooksAuthors]
GO
------Create Synonym for [dbo].[Publishers]------
CREATE SYNONYM [dbo].[Publishers] FOR [O_Skalska_Library].[dbo].[Publishers]
GO
------Create Synonym for [dbo].[Authors_log]------
CREATE SYNONYM [dbo].[Authors_log] FOR [O_Skalska_Library].[dbo].[Authors_log]
GO


