------Select Database------
USE [master]
GO

------DROP EXISTING DATABASE------
DROP DATABASE IF EXISTS O_Skalska_Library
GO
------Create New Database------
CREATE DATABASE [O_Skalska_Library]
GO

/*create new file group*/
USE [master]
GO

ALTER DATABASE [O_Skalska_Library] ADD FILEGROUP [Data]
GO

/*create new data file */
USE [master]
GO

ALTER DATABASE [O_Skalska_Library] 
ADD FILE ( NAME = N'O_Skalska_Library_data', 
    FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\O_Skalska_Library_data.mdf' , 
    SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
GO

/*set default file group*/
USE [O_Skalska_Library]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
	ALTER DATABASE [O_Skalska_Library] MODIFY FILEGROUP [Data] DEFAULT
GO

------Select New-created Database------
USE [O_Skalska_Library]
GO

USE O_Skalska_Library
GO

-------Create Authors_log table------

DROP TABLE IF EXISTS [Authors_log]
GO

CREATE TABLE [Authors_log]
(
 [operation_id]       INT NOT NULL IDENTITY ,
 [Author_Id_new]      INT NULL ,
 [Name_new]           VARCHAR(50) NULL ,
 [URL_new]            VARCHAR(50) NULL ,
 [Author_Id_old]      INT NULL ,
 [Name_old]           VARCHAR(50) NULL ,
 [URL_old]            VARCHAR(50) NULL ,
 [operation_type]     VARCHAR(50) NOT NULL ,
 [operation_datetime] DATETIME NOT NULL DEFAULT getdate(),
 ------Check BooksAuthors_id column------
 CONSTRAINT operation_type_chk CHECK ([operation_type] in ('I','D','U')
),

  ------Define Primary Key with Clustered Index------
 CONSTRAINT [PK_Authors_log] PRIMARY KEY CLUSTERED ([operation_id] ASC)
);
GO


-------Create Publishers table------

DROP TABLE IF EXISTS [Publishers]
GO

CREATE TABLE [Publishers]
(
 [Publisher_Id] INT NOT NULL IDENTITY,
 [Name]         VARCHAR(50) NOT NULL ,
 [URL]          VARCHAR(50) NOT NULL DEFAULT 'www.publisher_name.com',
 [inserted]     DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by]  VARCHAR(50) NOT NULL DEFAULT system_user,
 [updated]      DATE NULL ,
 [updated_by]   VARCHAR(50) NULL ,

 
  ------Define Primary Key with Clustered Index------

 CONSTRAINT [PK_Publishers] PRIMARY KEY CLUSTERED ([Publisher_Id] ASC),

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT Name_uk UNIQUE ([Name])
)
;
GO

------Create Sequence for Publisher_id-------
CREATE SEQUENCE publisher_seq 
AS INT
START WITH 10
INCREMENT BY 10
GO


-------Create Authors table------

DROP TABLE IF EXISTS [Authors]
GO

CREATE TABLE [dbo].[Authors]
(
 [Author_Id]   INT NOT NULL IDENTITY,
 [Name]        VARCHAR(50) NOT NULL ,
 [URL]         VARCHAR(50) NOT NULL DEFAULT 'www.author_name.com',
 [inserted]    DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by] TEXT NOT NULL DEFAULT SYSTEM_USER,
 [updated]     DATE NULL ,
 [updated_by]  TEXT NULL ,

 
  ------Define Primary Key with Clustered Index------

 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED ([Author_Id] ASC),

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT Name_Author_uk UNIQUE ([Name])
);
GO

------Create Sequence for Author_id-------
CREATE SEQUENCE author_seq 
AS INT
START WITH 100
INCREMENT BY 5
GO

-------Create Books table------

DROP TABLE IF EXISTS [Books]
GO

CREATE TABLE [Books]
(
 [ISBN]         VARCHAR(100) NOT NULL,
 [URL]          VARCHAR(50) NOT NULL ,
 [Price]        DECIMAL(18,0) NOT NULL DEFAULT 0,
 [inserted]     DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by]  VARCHAR(50) NOT NULL DEFAULT system_user,
 [updated]      DATE NULL ,
 [updated_by]   VARCHAR(50) NULL ,
 [Publisher_Id] INT NOT NULL ,
 ------Check price column------
 CONSTRAINT price_chk CHECK ([price] >=0),

 
  ------Define Primary Key with Clustered Index and Foreign key------
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED ([ISBN] ASC),
 CONSTRAINT [FK_80] FOREIGN KEY ([Publisher_Id])
 REFERENCES [Publishers]([Publisher_Id]),

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT URL_uk UNIQUE ([URL])
);
GO


-------Create BooksAuthors table------

DROP TABLE IF EXISTS [BooksAuthors]
GO

CREATE TABLE [BooksAuthors]
(
 [BooksAuthors_id] INT NOT NULL DEFAULT 1,
 [Seq_No]          INT NOT NULL DEFAULT 1,
 [inserted]        DATE NOT NULL DEFAULT GETDATE(),
 [inserted_by]     VARCHAR(50) NOT NULL DEFAULT system_user,
 [updated]         DATE NOT NULL DEFAULT GETDATE(),
 [updated_by]      VARCHAR(50) NULL ,
 [ISBN]            VARCHAR(100) NOT NULL ,
 [Author_Id]       INT NOT NULL ,
 ------Check BooksAuthors_id column------
 CONSTRAINT BooksAuthors_id_chk CHECK ([BooksAuthors_id] >=1),
 CONSTRAINT Seq_Nod_chk CHECK ([Seq_No] >=1),

   ------Define Primary Key with Clustered Index and Foreign key------

 CONSTRAINT [PK_BooksAuthors] PRIMARY KEY CLUSTERED ([BooksAuthors_id] ASC),
 CONSTRAINT [FK_84] FOREIGN KEY ([ISBN])
  REFERENCES [Books]([ISBN])
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
 CONSTRAINT [FK_92] FOREIGN KEY ([Author_Id])
  REFERENCES [dbo].[Authors]([Author_Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,

  ------Define Unique Key (alternate key, table level constraint)------
 CONSTRAINT Author_Id_uk UNIQUE ([Author_Id]),
 CONSTRAINT ISBN_uk UNIQUE ([ISBN])

);
GO


USE O_Skalska_Library
GO
------create delete-restriction trigger for Authors_log-------

DROP TRIGGER IF EXISTS [Authors_log_DELETE_Restriction]
GO

CREATE TRIGGER Authors_log_DELETE_Restriction
ON [Authors_log]
INSTEAD OF DELETE
AS
BEGIN
RAISERROR ('This table is protected',16,1)
ROLLBACK TRANSACTION
END 
GO

------create trigger on Update for Authors------

DROP TRIGGER IF EXISTS [AUTHORS_Updated]
GO

CREATE TRIGGER AUTHORS_Updated
ON [Authors]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[Authors]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
WHERE [Author_Id] IN (SELECT DISTINCT [Author_Id] FROM Inserted)
END 
GO

------create trigger on Update for Books------

DROP TRIGGER IF EXISTS [Books_Updated]
GO

CREATE TRIGGER Books_Updated
ON [Books]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[Books]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
WHERE [ISBN] IN (SELECT DISTINCT [ISBN] FROM Inserted)
END 
GO

------create trigger on Update for BooksAuthors------

DROP TRIGGER IF EXISTS [BooksAuthors_Updated]
GO

CREATE TRIGGER BooksAuthors_Updated
ON [BooksAuthors]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[BooksAuthors]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
WHERE [BooksAuthors_id] IN (SELECT DISTINCT [BooksAuthors_id] FROM Inserted)
END 
GO

------create trigger on Update for Publishers------

DROP TRIGGER IF EXISTS [Publishers_Updated]
GO

CREATE TRIGGER Publishers_Updated
ON [Publishers]
AFTER Update, Insert
AS
BEGIN
UPDATE [dbo].[Publishers]
SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER 
WHERE [Publisher_Id] IN (SELECT DISTINCT [Publisher_Id] FROM Inserted)
END 
GO

------update audit columns trigger for Authors_log / Insertion-----

DROP TRIGGER IF EXISTS [author_log_insert]
GO

CREATE TRIGGER author_log_insert
ON [Authors]
AFTER INSERT
AS
	INSERT INTO [Authors_log] 
 (
  [Author_Id_new]
 ,[Name_new]
 ,[URL_new]
 ,[operation_type]
 )
	SELECT author_id, [name], [url], 'I' FROM inserted
GO

------update audit columns trigger for Authors_log / Deleting-----

DROP TRIGGER IF EXISTS [author_log_Delete]
GO

CREATE TRIGGER author_log_Delete
ON [Authors]
AFTER Delete
AS
	INSERT INTO [Authors_log] 
 (
  [Author_Id_old]
 ,[Name_old] 
 ,[URL_old]
 ,[operation_type]
 )
	SELECT author_id, [name], [url], 'D' FROM deleted
GO

------update audit columns trigger for Authors_log / Updating-----

DROP TRIGGER IF EXISTS [author_log_Update]
GO

CREATE TRIGGER author_log_Update
ON [Authors]
AFTER Update
AS
	INSERT INTO [Authors_log]
	(
	[Author_Id_new]
   ,[Name_new]  
   ,[URL_new] 
   ,[Author_Id_old]
   ,[Name_old] 
   ,[URL_old] 
   ,[operation_type]
   ) 
   SELECT 
   inserted.[Author_Id]
  ,inserted.[Name]
  ,inserted.[URL]
  ,deleted.[Author_Id]
  ,deleted.[Name]
  ,deleted.[URL]
  ,'U'
  FROM inserted, deleted
 GO  


  ------Create 20 records for each table------
 USE O_Skalska_Library
 GO

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log

INSERT INTO O_Skalska_Library.dbo.Authors 
([Name], [URL])
VALUES 
('Author1' , 'www.author1.com'),
('Author2' , 'www.author2.com'),
('Author3' , 'www.author3.com'),
('Author4' , 'www.author4.com'),
('Author5' , 'www.author5.com'),
('Author6' , 'www.author6.com'),
('Author7' , 'www.author7.com'),
('Author8' , 'www.author8.com'),
('Author9' , 'www.author9.com'),
('Author10' , 'www.autho10.com'),
('Author11' , 'www.author11.com'),
('Author12' , 'www.author12.com'),
('Author13' , 'www.author13.com'),
('Author14' , 'www.author14.com'),
('Author15' , 'www.author15.com'),
('Author16' , 'www.author16.com'),
('Author17' , 'www.author17.com'),
('Author18' , 'www.author18.com'),
('Author19' , 'www.author19.com'),
('Author20' , 'www.author20.com')
GO

INSERT INTO O_Skalska_Library.dbo.Publishers
([Name], [URL])
VALUES
('Publisher1' , 'www.publisher1.com'),
('Publisher2' , 'www.publisher2.com'),
('Publisher3' , 'www.publisher3.com'),
('Publisher4' , 'www.publisher4.com'),
('Publisher5' , 'www.publisher5.com'),
('Publisher6' , 'www.publisher6.com'),
('Publisher7' , 'www.publisher7.com'),
('Publisher8' , 'www.publisher8.com'),
('Publisher9' , 'www.publisher9.com'),
('Publisher10' , 'www.publisher10.com'),
('Publisher11' , 'www.publisher11.com'),
('Publisher12' , 'www.publisher12.com'),
('Publisher13' , 'www.publisher13.com'),
('Publisher14' , 'www.publisher14.com'),
('Publisher15' , 'www.publisher15.com'),
('Publisher16' , 'www.publisher16.com'),
('Publisher17' , 'www.publisher17.com'),
('Publisher18' , 'www.publisher18.com'),
('Publisher19' , 'www.publisher19.com'),
('Publisher20' , 'www.publisher20.com')
GO


INSERT INTO O_Skalska_Library.dbo.Books
([ISBN], [Publisher_Id], [URL], [Price])
VALUES
('123456789', 1, 'www.publisher1.com', 10),
('123456788', 2, 'www.publisher2.com', 20),
('123456787', 3, 'www.publisher3.com', 30),
('123456786', 4, 'www.publisher4.com', 5),
('123456785', 5, 'www.publisher5.com', 2),
('123456784', 6, 'www.publisher6.com', 10),
('123456783', 7, 'www.publisher7.com', 12),
('123456782', 8, 'www.publisher8.com', 16),
('123456781', 9, 'www.publisher9.com', 105),
('123456780', 10, 'www.publisher10.com', 99),
('123456779', 11, 'www.publisher11.com', 23),
('123456769', 12, 'www.publisher12.com', 98),
('123456759', 13, 'www.publisher13.com', 20),
('123456749', 14, 'www.publisher14.com', 11),
('123456739', 15, 'www.publisher15.com', 26),
('123456729', 16, 'www.publisher16.com', 33),
('123456719', 17, 'www.publisher17.com', 77),
('123456709', 18, 'www.publisher18.com', 56),
('123456889', 19, 'www.publisher19.com', 32),
('123456989', 20, 'www.publisher20.com', 5)
GO

INSERT INTO O_Skalska_Library.dbo.BooksAuthors
([BooksAuthors_id], [ISBN] ,[Author_Id] ,[Seq_No])
VALUES
(1, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=1), 1, 1),
(2, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=2), 2, 2),
(3, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=3), 3, 3),
(4, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=4), 4, 4),
(5, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=5), 5, 5),
(6, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=6), 6, 6),
(7, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=7), 7, 7),
(8, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=8), 8, 8),
(9, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=9), 9, 9),
(10, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=10), 10, 10),
(11, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=11), 11, 11),
(12, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=12), 12, 12),
(13, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=13), 13, 13),
(14, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=14), 14, 14),
(15, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=15), 15, 15),
(16, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=16), 16, 16),
(17, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=17), 17, 17),
(18, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=18), 18, 18),
(19, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=19), 19, 19),
(20, (select [ISBN] from O_Skalska_Library.dbo.Books where O_Skalska_Library.dbo.Books.Publisher_Id=20), 20, 20)
GO

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log


USE O_Skalska_Library
GO

-----Update 10 records for each table-----

--CHECK BEFORE

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log

UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer1'
WHERE [Name] = 'Author1'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer2'
WHERE [Name] = 'Author2'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer3'
WHERE [Name] = 'Author3'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer4'
WHERE [Name] = 'Author4'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer5'
WHERE [Name] = 'Author5'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer6'
WHERE [Name] = 'Author6'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer7'
WHERE [Name] = 'Author7'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer8'
WHERE [Name] = 'Author8'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer9'
WHERE [Name] = 'Author9'
GO
UPDATE O_Skalska_Library.dbo.Authors 
SET [Name] = 'Writer10'
WHERE [Name] = 'Author10'
GO


UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product1'
WHERE [Name] = 'Publisher1'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product2'
WHERE [Name] = 'Publisher2'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product3'
WHERE [Name] = 'Publisher3'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product4'
WHERE [Name] = 'Publisher4'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product5'
WHERE [Name] = 'Publisher5'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product6'
WHERE [Name] = 'Publisher6'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product7'
WHERE [Name] = 'Publisher7'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product8'
WHERE [Name] = 'Publisher8'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product9'
WHERE [Name] = 'Publisher9'
GO
UPDATE O_Skalska_Library.dbo.Publishers 
SET [Name] = 'Product10'
WHERE [Name] = 'Publisher10'
GO


UPDATE O_Skalska_Library.dbo.Books 
SET [Price] = [Price]*1.5
GO

UPDATE O_Skalska_Library.dbo.BooksAuthors
SET [Seq_No] = 100+[Seq_No]
GO

-- CHECK AFTER Updating----

--SELECT * FROM O_Skalska_Library.dbo.Authors 
--SELECT * FROM O_Skalska_Library.dbo.Publishers
--SELECT * FROM O_Skalska_Library.dbo.Books
--SELECT * FROM O_Skalska_Library.dbo.AuthorsBooks
--SELECT * FROM O_Skalska_Library.dbo.Authors_log