USE O_Skalska_Library
GO

INSERT INTO [dbo].[Authors]
(
 Name 
,url
,birthday
,book_amount
,issue_amount
,total_edition
)
VALUES
('Arnold Bennett', 'https://en.wikipedia.org/wiki/Arnold_Bennett', '1867-05-27', 123, 20, 2500),
('A. E. Coppard ','https://en.wikipedia.org/wiki/A._E._Coppard','1878-04-03',5,5,10),
('Stephen Crane','https://en.wikipedia.org/wiki/Stephen_Crane','1871-08-26',30,58,2500),
('Roald Dahl','https://en.wikipedia.org/wiki/Roald_Dahl','1916-05-23',8,50,1200),
('Sir Arthur Conan Doyle','https://en.wikipedia.org/wiki/Conan_Doyle','1859-09-09',123, 20, 2500),
('Charles Dickens','https://en.wikipedia.org/wiki/Charles_Dickens','1812-07-12',30,58,2500),
('Daniel Etessami','https://en.wikipedia.org/wiki/Daniel_Etessami','1972-04-03',8,50,1200),
('F. Scott Fitzgerald','https://en.wikipedia.org/wiki/Fitzgerald','1896-10-11',123, 20, 2500),
('Ian Fleming','https://en.wikipedia.org/wiki/Ian_Fleming','1908-12-16',8,50,1200),
('C. S. Forester','https://en.wikipedia.org/wiki/S._Forester','1899-08-26',30,58,2500),
('Graham Greene','https://en.wikipedia.org/wiki/Graham_Greene','1904-03-30',8,50,1200),
('Thomas Hardy','https://en.wikipedia.org/wiki/Thomas_Hardy','1840-07-04',30,58,2500),
('Jerome K. Jerome','https://en.wikipedia.org/wiki/Jerome_K._Jerome','1859-02-28',123, 20, 2500),
('D. H. Lawrence','https://en.wikipedia.org/wiki/D._H._Lawrence','1885-01-12',8,50,1200),
('Katherine Mansfield','https://en.wikipedia.org/wiki/Mansfield','1888-12-27',123, 20, 2500)
GO

INSERT INTO [dbo].[Books]
(
 ISBN
,url
,Price
,Publisher_Id
,title
,edition
,published
)
VALUES
('1-86092-012-8', 'https://www.goodreads.com/book/show', 2.80, 21, 'Murder', 1, '2000-12-01'),
('1-86092-022-5','http://www.dccomics1.com/', 5.35, 21,'A Boy at Seven',5,'2015-01-03'),
('1-86092-010-1','http://abdopublishing2.com/', 2.30, 22,'The Higgler',2,'1998-12-01'),
('1-86092-025-x','http://www.si3.edu/', 4.56, 23,'The Open Boat',1,'2005-07-05'),
('1-86092-034-9','http://www.umass7.edu/umpress/', 10.23, 24,'The Great Switcheroo',3,'2003-12-01'),
('1-86092-003-9','http://www.fitzhenry8.ca/', 11.56, 25,'The Speckled Band',5,'2006-12-01'),
('1-86092-038-1','http://www.ecwpress6.com/', 20.36, 26,'The Signalman',1,'2003-12-01'),
('1-86092-031-4','http://www.signaturebooksg.com/', 5.32, 27,'The Five Orange Pips',2,'2002-12-01'),
('1-86092-039-X','http://www.fitzhenrdy.ca/', 4.56, 28,'Cormacks Black Monday/Geralds Day Off',2,'2001-12-01'),
('1-86092-033-0','http://www.signaturebdooks.com/', 8.96, 29,'The Diamond as Big as the Ritz',2,'1993-12-01'),
('1-86092-055-1','http://www.fwmediaf.com/', 9, 30,'From a View to a Kill',3,'2002-12-01'),
('1-86092-014-4','http://www.dccomihcs.com/', 11, 30,'The Hostage',1,'2003-12-01'),
('1-86092-021-7','http://www.dcecomics.com/', 15.23, 29,'A Chance for Mr Lever',1,'2008-12-01'),
('1-86092-045-4','http://www.ecwqpress.com/', 14.26, 28,'A Mere Interlude',3,'2011-12-01'),
('1-86092-050-0','http://feministjpress.org/', 86.78, 27,'The Dancing Partner: Clocks',4,'2017-12-01'),
('1-86092-007-1','http://www.douglas-mcintyyre.com/', 56.23, 26,'The Rocking-Horse Winner',5,'2014-12-01'),
('1-86092-005-5','http://www.signaturebodoks.com/', 23.23, 25,'Bliss Feuille Album',1,'2002-12-01'),
('1-86092-020-9','http://abdopublishing.com/', 5.5, 25,'Death on the Air',4,'2001-12-01'),
('1-86092-009-8','http://feministprdess.org/', 8.86, 25,'Mademoiselle Fifi',3,'2011-12-01'),
('1-86092-018-7','http://www.scvi.edu/', 7.8,30,'Christmas in Africa',2,'2013-12-01')
GO

INSERT INTO [dbo].[Publishers]
(
 Name
,url
,created
,country
,city
,book_amount
,issue_amount
,total_edition
)
VALUES
('ECW Press', 'http://www.ecwpress.com/', '1974-05-02', 'CA', 'Toronto', 500, 2000, 1000000),
('Fitzhenry & Whiteside Ltd.', 'http://www.fitzhenry.ca/', '1966-01-01', 'CA', 'Ontario', 68, 3000, 204000),
('Douglas & McIntyre', 'http://www.douglas-mcintyre.com/', '1971-02-13', 'CA', 'Vancouver', 700, 700, 490000),
('University of Massachusetts Press', 'http://www.umass.edu/umpress/', '1963-07-13', 'USA', 'Amherst', 100, 100, 10000),
('The Feminist Press', 'http://feministpress.org/', '1970-11-23', 'USA', 'NY', 200, 3000, 600000),
('F+W', 'http://www.fwmedia.com/', '1913-01-01', 'USA', 'NY', 5000, 5000, 25000000),
('Signature Books', 'http://www.signaturebooks.com/', '1980-04-04', 'USA', 'Salt Lake City', 786, 3500, 2688000),
('Smithsonian Institution Press', 'http://www.si.edu/', '1846-08-10', 'USA', 'Washington', 4586, 2000, 9172000),
('ABDO Publishing Company', 'http://abdopublishing.com/', '1985-03-18', 'USA', 'Edina', 2000, 780, 1560000),
('DC Comics', 'http://www.dccomics.com/', '1934-01-01', 'USA', 'Burbank', 500, 1000, 5000000)
GO

-----Check trigger-----
SELECT * FROM Authors_log