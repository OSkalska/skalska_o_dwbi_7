USE O_Skalska_Library
GO

-------Preparing tables for Updating-------

--DELETE Foreign key for BooksAuthors table 
ALTER TABLE [BooksAuthors]
DROP CONSTRAINT [FK_84]
GO

--DELETE Primary key for Books table
ALTER TABLE [Books]
DROP CONSTRAINT [PK_Books]
GO

-------Modifying Authors table------

ALTER TABLE [dbo].[Authors]
ADD 
 birthday DATE NULL, 
 book_amount SMALLINT NOT NULL,
 issue_amount SMALLINT NOT NULL,
 total_edition INT NOT NULL,
 CONSTRAINT authors_book_amount_default DEFAULT 0 FOR book_amount,
 CONSTRAINT authors_issue_amount_default DEFAULT 0 FOR issue_amount,
 CONSTRAINT authors_total_edition_default DEFAULT 0 FOR total_edition,
 CONSTRAINT authors_book_amount_check CHECK (book_amount >= 0),
 CONSTRAINT authors_issue_amount_check CHECK (book_amount >= 0),
 CONSTRAINT authors_total_edition_check CHECK (book_amount >= 0)
GO

-------Modifying Books table------

ALTER TABLE [Books]
ADD
 title VARCHAR(100) NOT NULL,
 edition SMALLINT NOT NULL,
 published DATE,
 issue SMALLINT IDENTITY,
 CONSTRAINT books_title_default DEFAULT 'Title' FOR title,
 CONSTRAINT books_edition_default DEFAULT 1 FOR edition,
 CONSTRAINT books_edition_check CHECK (edition >=1),
 ------SET new Primary key-------
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED ([issue] ASC)
GO

 -------Modifying Publishers table-------

ALTER TABLE [Publishers]
ADD
 created DATE NOT NULL,
 country VARCHAR(20) NOT NULL,
 City VARCHAR (50) NOT NULL,
 book_amount SMALLINT NOT NULL,
 issue_amount SMALLINT NOT NULL,
 total_edition INT NOT NULL,
 CONSTRAINT publishers_created_default DEFAULT 'Jan 1 1900' FOR created,
 CONSTRAINT publishers_country_default DEFAULT 'USA' FOR country,
 CONSTRAINT publishers_city_default DEFAULT 'NY' FOR City,
 CONSTRAINT publishers_book_amount_default DEFAULT 0 FOR book_amount,
 CONSTRAINT publishers_issue_amount_default DEFAULT 0 FOR issue_amount,
 CONSTRAINT publishers_total_edition_default DEFAULT 0 FOR total_edition,
 CONSTRAINT publishers_book_amount_check CHECK (book_amount >= 0),
 CONSTRAINT publishers_issue_amount_check CHECK (issue_amount >= 0),
 CONSTRAINT publishers_total_edition_check CHECK (total_edition >= 0)
GO

-------Modifying Authors_log table-------

ALTER TABLE [Authors_log]
ADD
 book_amount_old INT,
 issue_amount_old INT,
 total_edition_old INT,
 book_amount_new INT,
 issue_amount_new INT,
 total_edition_new INT
GO


------Authors_log trigger modification-----

ALTER TRIGGER author_log_insert
ON [Authors]
AFTER INSERT
AS
	INSERT INTO [Authors_log] 
 (
  [Author_Id_new]
 ,[Name_new]
 ,[URL_new]
 ,[operation_type]
 ,[book_amount_new]
 ,[issue_amount_new]
 ,[total_edition_new]
 )
	SELECT 
	[author_id]
   ,[name]
   ,[url]
   ,'I'
   ,[book_amount]
   ,[issue_amount]
   ,[total_edition]
   FROM inserted
GO


ALTER TRIGGER author_log_Delete
ON [Authors]
AFTER Delete
AS
	INSERT INTO [Authors_log] 
 (
  [Author_Id_old]
 ,[Name_old] 
 ,[URL_old]
 ,[operation_type]
 ,[book_amount_old]
 ,[issue_amount_old]
 ,[total_edition_old]
 )
	SELECT 
	[author_id]
   ,[name]
   ,[url]
   ,'D'
   ,[book_amount]
   ,[issue_amount]
   ,[total_edition] 
   FROM deleted
GO


ALTER TRIGGER author_log_Update
ON [Authors]
AFTER Update
AS
	INSERT INTO [Authors_log]
	(
	[Author_Id_new]
   ,[Name_new]  
   ,[URL_new] 
   ,[Author_Id_old]
   ,[Name_old] 
   ,[URL_old] 
   ,[operation_type]
   ,[book_amount_new]
   ,[issue_amount_new]
   ,[total_edition_new]
   ,[book_amount_old]
   ,[issue_amount_old]
   ,[total_edition_old]


   ) 
   SELECT 
   inserted.[Author_Id]
  ,inserted.[Name]
  ,inserted.[URL]
  ,deleted.[Author_Id]
  ,deleted.[Name]
  ,deleted.[URL]
  ,'U'
  ,inserted.[book_amount]
  ,inserted.[issue_amount]
  ,inserted.[total_edition]
  ,deleted.[book_amount]
  ,deleted.[issue_amount]
  ,deleted.[total_edition]

  FROM inserted, deleted
 GO  

-------Allow nested triggers-------

USE O_Skalska_Library
GO
EXEC sp_configure 'show advanced options', 1  
GO  
RECONFIGURE  
GO  
EXEC sp_configure 'nested triggers', 1  
GO  
RECONFIGURE  
GO  