USE O_Skalska_Library
GO

------Updating new columns according to existing records------
UPDATE [dbo].[Authors]
SET birthday = '1999-01-01', book_amount = 5, issue_amount = 10, total_edition = 15
WHERE Author_Id BETWEEN 1 and 20
GO

UPDATE [dbo].[Books]
SET title = 'new title', edition = 1000, published = '2018-07-20'
WHERE issue BETWEEN 1 AND 20
GO

UPDATE [dbo].[Publishers]
SET created = '2018-05-05', country = 'Ukraine', city = 'Lviv', book_amount = 10, issue_amount = 13, total_edition = 23
WHERE Publisher_Id BETWEEN 1 AND 20
GO



