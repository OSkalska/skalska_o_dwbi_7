USE [O_S_module_3]
GO

-----dirty reading-----

BEGIN TRANSACTION
GO

UPDATE [dbo].[orders]
SET shipping_type = 'bus'
WHERE order_id = 1
GO

WAITFOR DELAY '00:00:20'
GO

ROLLBACK
GO

SELECT shipping_type FROM [dbo].[orders] WHERE order_id = 1
GO

-----------------------------------------------------

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
GO

BEGIN TRANSACTION
GO

SELECT shipping_type FROM [dbo].[orders] WHERE order_id = 1
GO

COMMIT TRANSACTION
GO




--------nonrepeatable reading--------
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO

BEGIN TRANSACTION
GO

SELECT shipping_type FROM [dbo].[orders] WHERE order_id = 1
GO

WAITFOR DELAY '00:00:20'
GO

SELECT shipping_type FROM [dbo].[orders] WHERE order_id = 1
GO

COMMIT
GO

---------------------------------------------------


BEGIN TRANSACTION
GO

UPDATE [dbo].[orders]
SET shipping_type = 'car'
WHERE order_id = 1
GO

COMMIT TRANSACTION
GO


---------phantom reading---------

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
GO

BEGIN TRANSACTION
GO

SELECT * FROM [dbo].[customers]
GO 

WAITFOR DELAY '00:00:30'  

SELECT * FROM [dbo].[customers]
GO 

COMMIT
GO

----------------------------------------------

BEGIN TRANSACTION
GO

INSERT INTO [dbo].[orders]
           ([order_id] 
           ,[order_date]
           ,[weight]
		   ,[shipping_type] 
           ,[carrier_name]
		   ,[ship_address]
		   ,[ship_city] 
		   ,[ship_postal_code]
		   ,[ship_country]
		   ,[inserted_date] 
		   ,[updated_date] 
		   ,[customer_id]
		     )
     VALUES
           (5
           ,'2018-07-16'
           ,5
		   ,'train'
		   ,'intercity'
		   ,'kytajska 2'
		   ,'Lviv'
		   ,'79038'
		   ,'Gonduras'
		   ,DEFAULT
		   ,DEFAULT
		   ,'1'
		   )
GO

COMMIT TRANSACTION
GO