USE [labor_sql]
GO

--1.
SELECT DISTINCT maker, type 
FROM [dbo].[product]
WHERE type = 'laptop'
ORDER BY maker ASC;

--2.
SELECT model, ram, screen, price
FROM [dbo].[laptop]
WHERE price > 1000
ORDER BY ram ASC, price DESC;

--3.
SELECT *
FROM [dbo].[printer]
WHERE color = 'y'
ORDER BY price DESC;

--4.
SELECT model, speed, hd, cd, price
FROM [dbo].[pc]
WHERE cd IN ('12x', '24x') AND
price < 600
ORDER BY speed DESC;

--5.
SELECT name, class
FROM [dbo].[ships]
ORDER BY name ASC;

--6.
SELECT *
FROM [dbo].[pc]
WHERE speed >= 500 AND price < 800
ORDER BY price DESC;

--7.
SELECT *
FROM [dbo].[printer]
WHERE type != 'Matrix' AND price < 300
ORDER BY type DESC;

--8.
SELECT model, speed
FROM [dbo].[pc]
WHERE price BETWEEN 400 AND 600
ORDER BY hd ASC;

--9.
SELECT model, speed, hd, price
FROM [dbo].[laptop]
WHERE screen >= 12
ORDER BY price DESC;

--10.
SELECT model, type, price
FROM [dbo].[printer]
WHERE price < 300
ORDER BY type DESC;

--11.
SELECT model, ram, price
FROM [dbo].[laptop]
WHERE ram = 64
ORDER BY screen ASC;

--12.
SELECT model, ram, price
FROM [dbo].[pc]
WHERE ram > 64
ORDER BY hd ASC

--13.
SELECT model, speed, price
FROM [dbo].[pc]
WHERE speed BETWEEN 500 AND 750
ORDER BY hd DESC

--14.
SELECT *
FROM [dbo].[outcome_o]
WHERE out > 2000.00
ORDER BY date DESC

--15.
SELECT *
FROM [dbo].[income_o]
WHERE inc BETWEEN 5000.00 AND 10000.00
ORDER BY inc ASC

--16.
SELECT *
FROM [dbo].[income]
WHERE point = 1
ORDER BY inc ASC

--17.
SELECT *
FROM [dbo].[outcome]
WHERE point = 2
ORDER BY out ASC

--18.
SELECT *
FROM [dbo].[classes]
WHERE country = 'Japan'
ORDER BY type DESC

--19.
SELECT name, launched
FROM [dbo].[ships]
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC

--20.
SELECT ship, battle, result
FROM [dbo].[outcomes]
WHERE battle = 'Guadalcanal' AND
result != 'sunk' 
ORDER BY ship DESC

--21.
SELECT ship, battle, result
FROM [dbo].[outcomes]
WHERE result = 'sunk'
ORDER BY ship DESC

--22.
SELECT class, displacement
FROM [dbo].[classes]
WHERE displacement >= 40000
ORDER BY type ASC

--23.
SELECT trip_no, town_from, town_to
FROM [dbo].[trip]
WHERE town_from = 'London' OR town_to = 'London'
ORDER BY time_out ASC

--24.
SELECT trip_no, plane, town_from, town_to
FROM [dbo].[trip]
WHERE plane = 'TU-134'
ORDER BY time_out DESC

--25.
SELECT trip_no, plane, town_from, town_to
FROM [dbo].[trip]
WHERE plane != 'IL-86'
ORDER BY time_out ASC

--26.
SELECT trip_no, town_from, town_to
FROM [dbo].[trip]
WHERE town_from <> 'Rostov' AND town_to <> 'Rostov'
ORDER BY plane ASC

--27.
SELECT *
FROM [dbo].[pc]
WHERE model LIKE '%1%1%'

--28.
SELECT * 
FROM [dbo].[outcome]
WHERE date BETWEEN '2001-03-01' AND '2001-03-31'

--29.
SELECT *
FROM [dbo].[outcome_o]
WHERE DATEPART(dd, date) = 14

--30.
SELECT name
FROM [dbo].[ships]
WHERE name LIKE 'W%n'

--31.
SELECT name
FROM [dbo].[ships]
WHERE name LIKE '%e%e%'

--32.
SELECT name, launched
FROM [dbo].[ships]
WHERE name NOT LIKE '%a'

--33.
SELECT name
FROM [dbo].[battles]
WHERE name LIKE ('%' + ' ' + '%') AND name NOT LIKE '%c'

--34.
SELECT *
FROM [dbo].[trip]
WHERE time_out BETWEEN '12:00:00' AND '17:00:00'

--35.
SELECT *
FROM [dbo].[trip]
WHERE time_in BETWEEN '17:00:00' AND '23:00:00'

--36.
SELECT *
FROM [dbo].[trip]
WHERE time_in  BETWEEN '21:00:00' AND '23:59:59' OR 
      time_in BETWEEN '00:00:00' AND '10:00:00'

--37.
SELECT DISTINCT date
FROM [dbo].[pass_in_trip]
WHERE place LIKE '1%'

--38.
SELECT DISTINCT date
FROM [dbo].[pass_in_trip]
WHERE place LIKE '%c'

--39.
SELECT name AS Last_name
FROM [dbo].[passenger]
WHERE name LIKE ('%' + ' ' + 'C%')

--40.
SELECT name AS Last_name
FROM [dbo].[passenger]
WHERE name NOT LIKE ('%' + ' ' + 'J%')

--41.
SELECT CONCAT('������� ���� = ', AVG(price))
FROM [dbo].[laptop]

--42.
SELECT
    CONCAT('���: ', code) AS code
   ,CONCAT('������: ', model) AS model
   ,CONCAT('��������: ', speed) AS speed
   ,CONCAT('���"���: ', ram) AS ram
   ,CONCAT('��������: ', hd) AS hd
   ,CONCAT('�����: ', cd) AS cd
   ,CONCAT('����: ', price) AS price
FROM [dbo].[pc]

--43.
SELECT REPLACE(CONVERT(date, date), '-', '.')
FROM [dbo].[income]

--44.
SELECT 
    ship
   ,battle
   ,CASE result
       WHEN 'sunk'
	        THEN '����������'
       WHEN 'damaged'
	        THEN '�����������'
			ELSE '�������'
    END AS result
FROM [dbo].[outcomes]

--45.
SELECT
       trip_no
      ,date
	  ,id_psg
      ,CONCAT('���: ', left(place, CHARINDEX(place, place))) as row
      ,CONCAT('����: ', substring(place, CHARINDEX(place, place)+1, len(place)-(CHARINDEX(place, place)-1))) as place
FROM [dbo].[pass_in_trip]

--46.
SELECT 
       trip_no
      ,id_comp
	  ,plane
	  ,CONCAT(CONCAT('from ', TRIM(town_from)), CONCAT(' to ', TRIM(town_to))) AS flight
	  ,time_out
	  ,time_in
FROM [dbo].[trip]

--47.
SELECT 
      CONCAT(
	  LEFT(trip_no, 1), RIGHT(trip_no, 1)
     ,LEFT(id_comp, 1), RIGHT(id_comp, 1)
	 ,LEFT(plane, 1), RIGHT(TRIM(plane), 1)
	 ,LEFT(town_from, 1), RIGHT(TRIM(town_from), 1)
	 ,LEFT(town_to, 1), RIGHT(TRIM(town_to), 1)
	 ,LEFT(CONVERT(VARCHAR, time_out, 121), 1), (RIGHT(CONVERT(VARCHAR, time_out, 121), 1))
	 ,LEFT(CONVERT(VARCHAR, time_in, 121), 1), (RIGHT(CONVERT(VARCHAR, time_in, 121), 1))
	 ) AS merged_result
FROM [dbo].[trip]

--48.
SELECT maker, COUNT(model) AS different_models
FROM [dbo].[product]
WHERE type = 'pc'
GROUP BY maker
HAVING (COUNT(model) > 1)

--49.
SELECT 
  town
 ,COUNT(town) AS amount 
FROM
   (SELECT town_from AS town 
    FROM [dbo].[trip]
	UNION ALL
	SELECT town_to AS town 
	FROM [dbo].[trip]) 
AS town
GROUP BY town

--50.
SELECT type, COUNT(model) AS models
FROM [dbo].[printer]
GROUP BY type

--51.
SELECT model, cd, amount
FROM
(
SELECT cd, NULL AS model, COUNT(DISTINCT model) AS amount
FROM [dbo].[pc]
GROUP BY (cd)
UNION ALL
SELECT NULL AS cd, model, COUNT(DISTINCT cd) AS amount
FROM [dbo].[pc]
GROUP BY (model)
) a

--52.
SELECT trip_no, RIGHT(CONVERT(VARCHAR, (time_in - time_out), 121), 12) AS duration
FROM [dbo].[trip]

--53.
SELECT 
  COALESCE(CONVERT(VARCHAR, point), 'ALL') AS POINT
 ,COALESCE(CONVERT(VARCHAR, date, 121), 'TOTAL') AS DATE
 ,SUM(out) AS SUM
 ,MIN(out) AS MIN
 ,MAX(out) AS MAX
FROM [dbo].[outcome]
GROUP BY point, date WITH ROLLUP

--54.
SELECT trip_no, left(place, CHARINDEX(place, place)) AS row, COUNT(left(place, CHARINDEX(place, place))) AS places
FROM [dbo].[pass_in_trip]
GROUP BY trip_no, left(place, CHARINDEX(place, place))
ORDER BY trip_no ASC

--55.
DROP TABLE IF EXISTS #split
GO
SELECT left(name, CHARINDEX(' ', name)) as firstname,
substring(name, CHARINDEX(' ', name)+1, len(name)-(CHARINDEX(' ', name)-1)) as lastname
INTO #split
FROM [dbo].[passenger]
GO
SELECT COUNT(lastname) AS Passengers
FROM #split
WHERE substring(lastname, 1, 1) IN ('S', 'B', 'A')
GO
DROP TABLE IF EXISTS #split
GO

--The END--
