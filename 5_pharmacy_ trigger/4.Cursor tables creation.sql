USE triger_fk_cursor;

DECLARE 
       @Surname varchar(30), 
	   @Name varchar(30),
       @First_table nvarchar(max), 
	   @Second_table nvarchar(max),
       @Count int;

DECLARE result_table CURSOR
	FOR SELECT 
	          [surname] 
			 ,[name] 
		FROM [dbo].[employee];

OPEN result_table
	FETCH NEXT 
	FROM result_table 
	INTO @Surname
	    ,@Name;
	WHILE 
	@@FETCH_STATUS = 0
	BEGIN
		SET @Second_table = '';
		SET @Count = ROUND(((RAND()*10)+1), 0); 
		SET @First_table = 'CREATE TABLE ' + @Surname + @Name + ' ('
		WHILE @Count > 0
		BEGIN
			SET @Second_table = @Second_table + 'column' + CONVERT(char(1), @Count) + ' INT, ';
			SET @Count = @Count - 1;
		END
		SET @Second_table = LEFT(@Second_table, LEN(@Second_table) - 1) 
		SET @Second_table = @Second_table + ')'
		EXEC(@First_table + @Second_table)
		FETCH NEXT 
		FROM result_table 
		INTO @Surname
		    ,@Name;
	END;
CLOSE result_table;
DEALLOCATE result_table;
