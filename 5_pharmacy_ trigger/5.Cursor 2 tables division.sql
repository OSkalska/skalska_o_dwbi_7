----script for dynamically creating tables-----
USE triger_fk_cursor;

DECLARE @first_table AS VARCHAR(18)
       ,@second_table AS VARCHAR(18)
       ,@action_1 AS VARCHAR(max)
	   ,@action_2 AS VARCHAR(max)
       ,@definition AS VARCHAR(max)
       ,@row AS VARCHAR(max)
       ,@count AS VARCHAR(max)
	   ,@id AS INT
	   ,@surname AS VARCHAR(30)
	   ,@name AS VARCHAR(30)
	   ,@midle_name AS VARCHAR(30)
       ,@identity_number AS VARCHAR(10)
	   ,@passport AS VARCHAR(10)
	   ,@experience AS DECIMAL(10, 1)
       ,@birthday AS DATE
	   ,@post AS VARCHAR(15)
	   ,@pharmacy_id AS INT;

DECLARE insertion CURSOR
FOR
	SELECT 
	      [id]
		 ,[surname]
		 ,[name]
		 ,[midle_name]
		 ,[identity_number]
		 ,[passport]
		 ,[experience]
		 ,[birthday]
		 ,[post]
		 ,[pharmacy_id]
	FROM [dbo].[employee];

SET @definition='(
                  id INT NOT NULL
                 ,surname VARCHAR(30) NOT NULL
				 ,name VARCHAR(30) NOT NULL
				 ,midle_name VARCHAR(30)
				 ,identity_number VARCHAR(10)
				 ,passport VARCHAR(10)
				 ,experience DECIMAL(10, 1)
				 ,birthday DATE
                 ,post VARCHAR(15) NOT NULL
				 ,pharmacy_id INT
				 )';

SET @count='(
             id
			,surname
			,name
			,midle_name
			,identity_number
			,passport
			,experience
			,birthday
			,post
			,pharmacy_id
			)'

SET @first_table='[Table1_'+ CAST(CAST(CURRENT_TIMESTAMP AS TIME(0)) AS VARCHAR(8))+']';
SET @second_table='[Table2_'+ CAST(CAST(CURRENT_TIMESTAMP AS TIME(0)) AS VARCHAR(8))+']';
SET @action_1='create table ' + @first_table + @definition +' '+
			  'create table ' + @second_table + @definition +';';
EXEC(@action_1);

OPEN insertion;
FETCH NEXT 
FROM insertion 
INTO @id
    ,@surname
	,@name
	,@midle_name
	,@identity_number
	,@passport
	,@experience
	,@birthday
	,@post
	,@pharmacy_id;
WHILE 
     @@FETCH_STATUS=0
	 BEGIN
		SET @row=RTRIM(cast(@id AS VARCHAR(5)))+','+''''+RTRIM(@surname)+''''+','+''''+RTRIM(@name)+''''+','
			+''''+RTRIM(@midle_name)+''''+','+''''+RTRIM(@identity_number)+''''+','
			+''''+RTRIM(@passport)+''''+','+RTRIM(cast(@experience AS VARCHAR(5)))+','+
			''''+RTRIM(cast(@birthday AS VARCHAR(10)))+''''+','
			+''''+RTRIM(@post)+''''+','+RTRIM(cast(@pharmacy_id AS VARCHAR(5)))

		IF RAND() > 0.5
			BEGIN
				SET @action_2='INSERT INTO '+@first_table+@count+' VALUES('+@row+')'
			END
		ELSE
			BEGIN
				SET @action_2='INSERT INTO '+@second_table+@count+' VALUES('+@row+')'
			END
		EXEC(@action_2)

		FETCH NEXT 
		FROM insertion 
		INTO @id
		    ,@surname
			,@name
			,@midle_name
			,@identity_number
			,@passport
			,@experience
			,@birthday
			,@post
			,@pharmacy_id;
	 END;

CLOSE insertion;
DEALLOCATE insertion;

