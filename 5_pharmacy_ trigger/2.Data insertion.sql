USE triger_fk_cursor
GO

------Populate existing tables------
INSERT INTO [dbo].[zone]
VALUES ('head')
      ,('arms')
	  ,('legs')
	  ,('spine')
	  ,('bones')
	  ,('intestine') 
	  ,('teeth')
	  ,('lungs')
	  ,('stomach')
GO


INSERT INTO [dbo].[post]
VALUES ('Doctor')
      ,('Engineer')
	  ,('Saler')
	  ,('Manager')
	  ,('Driver')
GO

INSERT INTO [dbo].[pharmacy]
    (
     name
    ,building_number
    ,www
    ,street
    )
VALUES 
('1', '26','http://store-ds.com.ua/','Franka'),
('2', '8','http://store-ds.com.ua/', 'Franka'),
('3', '1','http://store-ds.com.ua/', 'Khmelnytskogo'),
('4', '11','http://store-ds.com.ua/', 'Bazarna'),
('5', '1','http://store-ds.com.ua/', 'Velukogo'),
('6', '10','http://store-ds.com.ua/', 'Vugovskogo'),
('7', '9a','http://store-ds.com.ua/', 'Haydamatska'),
('8', '19','http://store-ds.com.ua/', 'Halytska')
GO


INSERT INTO [dbo].[employee]
     (
	  surname
	 ,name
	 ,midle_name
	 ,identity_number
	 ,passport
	 ,experience
	 ,birthday
	 ,post
	 ,pharmacy_id
	 )
VALUES
('Skalska', 'Olena', 'Mychaylivna', '1234567892','KB123456', 20, '1986-05-12', 'Doctor' , 8),
('Skalsky', 'Marko', 'Yriyovuch', '6549873215', 'KB654987', 25, '1982-02-03','Manager', 8),
('Skalsky', 'Stanislav', 'Markovych','5698713235','KB112365', 7, '1995-12-17','Driver', 7),
('Skalsky', 'Artem', 'Markovych', '6549873215', 'KB569872', 5, '1990-09-09','Driver', 2),
('Hrytsyk', 'Stanislav', 'Zinoviyovych', '5469874562', 'KB329004', 15, '1983-08-30','Saler', 3),
('Hrytsyk', 'Marta', 'Orestivna', '9874523698', 'KB556632', 5, '1986-02-11', 'Engineer', 3),
('Hrytsyk', 'Margaryta', 'Stanislavivna', '9874236985', 'KB442369', 8, '1992-05-27', 'Driver', 1),
('Hrytsyk', 'Marko', 'Stanislavovych', '5563214789', 'KB789654', 5, '1997-08-16', 'Doctor', 4)

GO

INSERT INTO [dbo].[medicine]
    (
	 name
    ,ministry_code
	,recipe
	,narcotic
	,psychotropic
	)
VALUES
('Ventolin', 'UA456873', 0, 0, 0),
('Ketanol', 'UA546321', 0, 0, 1),
('Dykloberl', 'UA558963', 0, 0, 0),
('Hidazepam', 'UA321569', 1, 0, 1),
('Purgen', 'UA569874', 0, 0, 0),
('Sybazon', 'UA659874', 1, 0, 1),
('Tramadol', 'UA569874', 1, 1, 1),
('Kortaksyfan', 'UA554123', 1, 1, 1)
GO


INSERT INTO [dbo].[medicine_zone]
     (
	  medicine_id
	 ,zone_id
	 )
VALUES 
 (1,1)
,(2,1)
,(3,3)
,(4,5)
,(5,9)
,(6,7)
,(7,2)
,(8,4)
GO 

INSERT INTO [dbo].[pharmacy_medicine]
    (
	 pharmacy_id
	,medicine_id
	)
VALUES 
 (1,2)
,(1,3)
,(1,5)
,(1,8)
,(2,1)
,(2,2)
,(2,3)
,(2,7)
,(3,1)
,(3,5)
,(3,6)
,(4,8)
,(4,3)
,(4,5)
,(5,1)
,(5,2)
,(6,6)
,(6,7)
,(7,1)
,(7,3)
,(8,5)
,(8,2)
GO