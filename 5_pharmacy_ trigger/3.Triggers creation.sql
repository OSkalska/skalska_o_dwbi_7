USE triger_fk_cursor
GO

CREATE TRIGGER medicine_code_trigger 
ON [dbo].[medicine]
AFTER INSERT,UPDATE
AS
IF ((SELECT ministry_code FROM inserted) NOT LIKE '[a-z]' + '[a-z]' + '-' + '[0-9]' + '[0-9]' + '[0-9]' + '-' + '[0-9]' + '[0-9]') OR
   ((SELECT ministry_code FROM inserted) LIKE 'm%') OR
   ((SELECT ministry_code FROM inserted) LIKE '_m%') OR
   ((SELECT ministry_code FROM inserted) LIKE 'p%') OR
   ((SELECT ministry_code FROM inserted) LIKE '_p%')
	ROLLBACK TRANSACTION
GO


CREATE TRIGGER id_trigger 
ON [dbo].[employee]
AFTER INSERT,UPDATE
AS
IF ((SELECT identity_number FROM inserted) LIKE '%00')
	ROLLBACK TRANSACTION
	PRINT 'ID CANNOT BE ENDED WITH 00';
GO


CREATE TRIGGER medicine_zone_trigger 
ON [dbo].[medicine_zone]
AFTER INSERT, UPDATE
AS
  IF 
   ((SELECT medicine_id FROM inserted) NOT IN (SELECT id FROM [dbo].[medicine])) OR
   ((SELECT zone_id FROM inserted) NOT IN (SELECT id FROM [dbo].[zone]))
		ROLLBACK TRANSACTION
		PRINT 'INCORRECT FOREIGN KEY'
GO

CREATE TRIGGER pharmacy_medicine_trigger 
ON [dbo].[pharmacy_medicine]
AFTER INSERT,UPDATE
AS
  IF 
   ((SELECT pharmacy_id FROM inserted) NOT IN (SELECT id FROM [dbo].[pharmacy])) OR 
   ((SELECT medicine_id FROM inserted) NOT IN (SELECT id FROM [dbo].[medicine]))
		ROLLBACK TRANSACTION
		PRINT 'INCORRECT FOREIGN KEY'
GO

CREATE TRIGGER employee_trigger 
ON [dbo].[employee]
AFTER INSERT,UPDATE
AS
  IF ((SELECT pharmacy_id FROM inserted) NOT IN (SELECT id FROM [dbo].[pharmacy]))
		ROLLBACK TRANSACTION
		PRINT 'INCORRECT FOREIGN KEY'
  IF ((SELECT post FROM inserted) NOT IN (SELECT post FROM [dbo].[post]))
		ROLLBACK TRANSACTION
		PRINT 'INCORRECT FOREIGN KEY'
GO

CREATE TRIGGER pharmacy_trigger 
ON [dbo].[pharmacy]
AFTER INSERT,UPDATE
AS
  IF ((SELECT street FROM inserted) NOT IN (SELECT street FROM [dbo].[street]))
		ROLLBACK TRANSACTION
		PRINT 'INCORRECT FOREIGN KEY'
GO

CREATE TRIGGER post_trigger 
ON [dbo].[post]
AFTER UPDATE,DELETE
AS
ROLLBACK TRANSACTION
PRINT 'THIS OPERATION IS PROHIBITED FOR CURRENT TABLE'
GO

CREATE TRIGGER street_trigger 
ON [dbo].[street]
AFTER UPDATE
AS
UPDATE [dbo].[pharmacy]
SET street = (SELECT street FROM inserted)
WHERE street = (SELECT street FROM deleted)
GO

CREATE TRIGGER street_trigger_deletion 
ON [dbo].[street]
AFTER DELETE
AS
UPDATE [dbo].[pharmacy]
SET street = NULL
WHERE street = (SELECT street FROM deleted)
GO

CREATE TRIGGER zone_trigger_deletion 
ON [dbo].[zone]
AFTER DELETE
AS
DELETE [dbo].[medicine_zone]
WHERE zone_id = (SELECT id FROM deleted)
GO 

CREATE TRIGGER pharmacy_trigger_deletion 
ON [dbo].[pharmacy]
AFTER DELETE
AS
UPDATE [dbo].[pharmacy_medicine]
SET pharmacy_id = NULL
WHERE pharmacy_id = (SELECT id FROM deleted)
GO

CREATE TRIGGER medicine_trigger_deletion 
ON [dbo].[medicine]
AFTER DELETE
AS
ROLLBACK TRANSACTION
PRINT 'THIS OPERATION IS PROHIBITED FOR CURRENT TABLE'
GO



